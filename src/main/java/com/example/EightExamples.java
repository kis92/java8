package com.example;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by user on 11.01.17.
 */
public class EightExamples {
    public static void main(String[] args) {

        Arrays.stream(new int[] {1,2,3,4})
                .map(n->2*n)
                .average()
                .ifPresent(System.out::println);

        Stream.of("c1", "c2", "c3")
                .map(s->s.substring(1))
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(System.out::println);

        Stream.of(1,2,3,4)
                .map(i->"c"+i)
                .forEach(System.out::println);

        List<String> list = Arrays.asList("aa1", "cc2", "cc1", "aa2", "bb1");
        list.stream()
                .filter(s -> s.startsWith("a")) //промежуточная
                .map(String::toUpperCase) //промежуточная
                .sorted() //промежуточная
                .forEach(System.out::println); //терминальная

        list.stream()
                .findFirst()
                .ifPresent(System.out::println);

        list.stream()
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return s.startsWith("a");
                })
                .sorted((s1,s2)->{
                    System.out.printf("sort: %s;%s \n", s1,s2);
                    return s1.compareTo(s2);
                })
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .forEach(s-> System.out.println("forEach = " + s));

        List<Person> persons = Arrays.asList(new Person("Andrew", 20), new Person("Igor", 23), new Person("Ira", 23), new Person("Vitia", 12));

        List<Person> filtered = persons
                .stream()
                .filter(person -> person.getName().startsWith("I"))
                .collect(Collectors.toList());
        System.out.println("filtered = " + filtered);

        Map<Integer, List<Person>> personsByAge = persons
                .stream()
                .collect(Collectors.groupingBy(Person::getAge));
        System.out.println("personsByAge = " + personsByAge);

        double averageAgePersons = persons.stream().collect(Collectors.averagingInt(Person::getAge));
        System.out.println("averageAgePersons = " + averageAgePersons);

        IntSummaryStatistics ageSummary = persons.stream().collect(Collectors.summarizingInt(Person::getAge));
        System.out.println("ageSummary = " + ageSummary);

        String phraze = persons
                .stream()
                .filter(person -> person.getAge()>=18)
                .map(Person::getName)
                .collect(Collectors.joining(" and ", "In Germany ", " older 18"));
        System.out.println("phraze = " + phraze);

        Map<Integer, String> mapPersons = persons
                .stream()
                .collect(Collectors.toMap(
                        Person::getAge,
                        Person::getName,
                    (name1, name2)->name1+";"+name2)
                );
        System.out.println("mapPersons = " + mapPersons);

        Collector<Person, StringJoiner, String> personNameCollector = Collector.of(
                ()->new StringJoiner(" | "),
                (j,p)->j.add(p.getName().toUpperCase()),
                StringJoiner::merge,
                StringJoiner::toString
        );
        String names = persons.stream().collect(personNameCollector);
        System.out.println("names = " + names);

/////////////////////////FlatMap
        List<Foo> foos = new ArrayList<>();
        IntStream.range(1,4).forEach(i->foos.add(new Foo("Foo"+i)));
        foos.forEach(
                f->IntStream.range(1,4)
                        .forEach(
                                i->f.getBars().add(
                                        new Bar("Bar"+i+"<-"+f.getName())
                                )
                        )
        );
        foos.stream()
                .flatMap(f->f.getBars().stream())
                .forEach(b-> System.out.println("b.getName() = " + b.getName()));

//////////////////////////Optional
        Outer outer = new Outer();
        if(outer!=null && outer.nested!=null && outer.nested.inner!=null){
            System.out.println(outer.nested.inner.foo);
        }
        //or
        Optional.of(new Outer())
                .flatMap(o->Optional.ofNullable(o.nested))
                .flatMap(n->Optional.ofNullable(n.inner))
                .flatMap(i->Optional.ofNullable(i.foo))
                .ifPresent(System.out::println);

        ///////////////Reduce сочетание всех елементов потока в один результат
        persons.stream()
                .reduce((p1,p2)->p1.getAge()>p2.getAge()?p1:p2)
                .ifPresent(System.out::println);
        Person result =
                persons.stream()
                .reduce(new Person("",0),(p1,p2)->{
                    p1.age+=p2.age;
                    p1.name+=p2.name;
                    return p1;
                });
        System.out.format("name=%s; age=%s", result.getName(), result.getAge());

        Integer ageSum =
                persons.stream()
                .reduce(0,
                        (sum,p)->sum+=p.age,
                        (sum1,sum2)->sum1+sum2
                );
        System.out.println("ageSum = " + ageSum);



    }
    static class Outer{
        Nested nested;
    }
    static class Nested{
        Inner inner;
    }
    static class Inner{
        String foo;
    }


    static class Foo{
        private String name;
        private List<Bar> bars = new ArrayList<>();

        Foo(String name){
            this.name = name;
        }

        public List<Bar> getBars() {
            return bars;
        }

        public String getName() {
            return name;
        }
    }

    static class Bar{
        private String name;
        Bar(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }


    static class Person {
        private String name;
        private int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}

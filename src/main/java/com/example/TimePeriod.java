package com.example;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by user on 27.01.17.
 */
public class TimePeriod {
    public static void main(String[] args) {
        LocalDate after = LocalDate.of(2016, 8, 07);
        System.out.println("after = " + after);
        LocalDate before = LocalDate.of(2017, 1, 05);
        System.out.println("before = " + before);
        System.out.println("months = " + fillMonthPeriods(after, before));
        System.out.println("quartals = " + fillQuartalPeriods(after, before));
    }

    private static List<DatePeriod> fillQuartalPeriods(LocalDate after, LocalDate before) {
        List<DatePeriod> datePeriods = new LinkedList<>();
        ///////////////////after = после
        DatePeriod datePeriodOfQuartalAfter = datePeriodOfQuartal(after);
        LocalDate quartalEndOfAfter = datePeriodOfQuartalAfter.getEnd();
        LocalDate quartalStartOfAfter = datePeriodOfQuartalAfter.getStart();
        ///////////////////before = до
        DatePeriod datePeriodOfQuartalBefore = datePeriodOfQuartal(before);
        LocalDate quartalStartOfBefore = datePeriodOfQuartalBefore.getStart();
        LocalDate quartalEndOfBefore = datePeriodOfQuartalBefore.getEnd();

        Period period = Period.ofMonths(3);
        LocalDate tmpStart = LocalDate.from(quartalStartOfAfter);
        while (tmpStart.isBefore(quartalEndOfBefore)) {
            LocalDate tmpEnd = LocalDate.from(tmpStart).plus(period).minusDays(1);
            datePeriods.add(new DatePeriod(tmpStart, tmpEnd));
            tmpStart = tmpEnd.plusDays(1);
        }

        datePeriods.get(0).setStart(after);
        datePeriods.get(datePeriods.size() - 1).setEnd(before);

        return datePeriods;
    }
    private static DatePeriod datePeriodOfQuartal(LocalDate data){
        LocalDate quartalEndOfData = null;
        LocalDate quartalStartOfData = null;

        LocalDate quartalOneStart = LocalDate.of(data.getYear(), Month.JANUARY, 1);
        LocalDate quartalOneEnd = LocalDate.of(data.getYear(), Month.MARCH, Month.MARCH.maxLength());

        LocalDate quartalTwoStart = LocalDate.of(data.getYear(), Month.APRIL, 1);
        LocalDate quartalTwoEnd = LocalDate.of(data.getYear(), Month.JUNE, Month.JUNE.maxLength());

        LocalDate quartalThreeStart = LocalDate.of(data.getYear(), Month.JULY, 1);
        LocalDate quartalThreeEnd = LocalDate.of(data.getYear(), Month.SEPTEMBER, Month.SEPTEMBER.maxLength());

        LocalDate quartalFourStart = LocalDate.of(data.getYear(), Month.OCTOBER, 1);
        LocalDate quartalFourEnd = LocalDate.of(data.getYear(), Month.DECEMBER, Month.DECEMBER.maxLength());

        if (data.isBefore(quartalOneEnd) && data.isAfter(quartalOneStart)) {
            quartalEndOfData = quartalOneEnd;
            quartalStartOfData = quartalOneStart;
        }
        if (data.isBefore(quartalTwoEnd) && data.isAfter(quartalTwoStart)) {
            quartalEndOfData = quartalTwoEnd;
            quartalStartOfData = quartalTwoStart;
        }
        if (data.isBefore(quartalThreeEnd) && data.isAfter(quartalThreeStart)) {
            quartalEndOfData = quartalThreeEnd;
            quartalStartOfData = quartalThreeStart;
        }
        if (data.isBefore(quartalFourEnd) && data.isAfter(quartalFourStart)) {
            quartalEndOfData = quartalFourEnd;
            quartalStartOfData = quartalFourStart;
        }
        return new DatePeriod(quartalStartOfData, quartalEndOfData);
    }

    private static List<DatePeriod> fillMonthPeriods(LocalDate after, LocalDate before) {
        List<DatePeriod> datePeriods = new LinkedList<>();

        Period period = Period.ofMonths(1);
        LocalDate tmpStart = LocalDate.from(after).withDayOfMonth(1);
        while (tmpStart.isBefore(before)) {
            LocalDate tmpEnd = LocalDate.from(tmpStart).withDayOfMonth(tmpStart.getMonth().maxLength());
            datePeriods.add(new DatePeriod(tmpStart, tmpEnd));
            tmpStart = tmpStart.plus(period);
        }

        datePeriods.get(0).setStart(after);
        datePeriods.get(datePeriods.size() - 1).setEnd(before);
        return datePeriods;
    }

    static class DatePeriod {
        private LocalDate start, end;

        public DatePeriod(LocalDate start, LocalDate end) {
            this.start = start;
            this.end = end;
        }

        public LocalDate getStart() {
            return start;
        }

        public void setStart(LocalDate start) {
            this.start = start;
        }

        public LocalDate getEnd() {
            return end;
        }

        public void setEnd(LocalDate end) {
            this.end = end;
        }

        @Override
        public String toString() {
            return "DatePeriod{" +
                    "start=" + start +
                    ", end=" + end +
                    '}';
        }
    }
}
